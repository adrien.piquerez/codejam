import scala.io.Source

object Solution extends App {
  Source.stdin.getLines.drop(1).grouped(2).zipWithIndex.foreach{
    case (Seq(_, lydia), x) =>
      println(s"Case #${x + 1}: ${lydia.map(c => if(c == 'E') 'S' else 'E').mkString}")
  }
}