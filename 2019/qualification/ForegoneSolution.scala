import scala.io.Source

object Solution extends App {
  Source.stdin.getLines.drop(1).zipWithIndex.foreach { case (n, x) =>
    val t = n.toStream.map(c => if (c == '4') ('3', '1') else (c, '0'))
    val a = t.map(_._1).mkString
    val b = t.map(_._2).mkString.toInt
    println(s"Case #${x + 1}: $a $b")
  }
}
